import 'dart:core';
import 'dart:ui' as ui;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class NewOffset implements Comparable<NewOffset> {
  Offset a1 = new Offset(0,0), a2 = new Offset(0,0);
  NewOffset(this.a1, this.a2);

  @override
  int compareTo(NewOffset other) {
    return  (this.a1.dx).compareTo(other.a1.dx);
  }
  String toString(){
    return [this.a1.dx.toInt(), this.a1.dy.toInt(), this.a2.dx.toInt(), this.a2.dy.toInt()].toString();
  }
}

class Game extends CustomPainter{
  static int frameNum = 0;
  static bool isXMoveAble= true;
  static bool isYMoveAble= true;
  static double currentX = 0;
  static double currentY = 0;
  List<NewOffset> linesOffset = new List<NewOffset>();
  double accuX, accuY,pX=5,pY=5;
  Size bigMe;
  static int frameText = 0;
  void moveBall(Size size, double nextX, double nextY){
    double speed = 200;
    double cX = nextX * 10 * -1;
    double cY = nextY * 10;
    //for left right up down max
    if(currentX == 0 && currentY == 0){
      currentX = size.width/2.5;
      currentY = size.height/2.5;
    }
    if(currentX+cX/size.width*speed<0) {
      isXMoveAble = false;
    }
    if(currentX+cX/size.width*speed>size.width) {
      isXMoveAble = false;
    }
    if(currentY+cY/size.height*speed<10) {
      isYMoveAble = false;
    }
    if(currentY+cY/size.height*speed>size.height) {
      isYMoveAble = false;
    }
    //
    // check if they are line exist
    double iY1 = 0, iY2 = size.height, iX1 = 0, iX2 = size.width;
    List<double> rawX = new List<double>();
    List<double> rawY = new List<double>();
    //eliminate other lines that not in current Y
    rawX.add(0);
    rawY.add(0);
    for(int i=0; i<linesOffset.length;i++) {
      //start                                 //stop
      linesAX = linesOffset.elementAt(i).a1.dx;
      linesAY = linesOffset.elementAt(i).a1.dy;
      linesBX = linesOffset.elementAt(i).a2.dx;
      linesBY = linesOffset.elementAt(i).a2.dy;
      //sort the next
      //if they are in Y range
      if((currentY+lines.strokeWidth>linesAY && currentY<linesBY+lines.strokeWidth)){
        rawX.add(linesAX); //we can make validation here to check wether to use AX or BX if they are closer
      }
      //if they are in X range add to rawY for sorting
      if(currentX+lines.strokeWidth>linesAX && currentX<linesBX+lines.strokeWidth){
        rawY.add(linesBY); //same here
        // print([currentY.toInt(), currentX.toInt(), linesAX.toInt(), linesBX.toInt()]);
      }
    }
    rawY.add(size.height);
    rawX.add(size.width);

    for(double x in rawX){
      if(iX1<x && currentX>x){
        iX1 = x;
      }
      if(currentX<x && x<iX2){
        iX2 = x;
      }
    }
    for(double y in rawY){
      if(iY1<y && currentY>y){
        iY1 = y;
      }
      if(currentY<y && y<iY2){
        iY2 = y;
      }
    }

    // print([ "X: ${currentX} Candidate [${iX1}, ${iX2}], Y: ${currentY} candidate [${iY1}, ${iY2}]"]);

    //sort
    double nextCX = cX/size.width*speed;
    //check if the speed is more, then it can move
    if(isXMoveAble && (cX<-2 || cX>2)){
      bool xcon1 = currentX+nextCX>iX1+lines.strokeWidth+1;
      bool xcon2 = currentX+nextCX+lines.strokeWidth+1<iX2;
      if((xcon1) && (xcon2)){
        currentX+= nextCX;
      }else{
        if(!xcon2) {
          // currentX -= (currentX + nextCX - (iX1 + lines.strokeWidth + 1)) - nextCX;
          // print("HWO");
        }
      }
    }
    double nextCY = cY/size.height*speed;
    if(isYMoveAble && (cY<-2 || cY>2)){

      bool ycon1 = currentY+nextCY>iY1+lines.strokeWidth+1;
      bool ycon2 = currentY+nextCY+lines.strokeWidth+1<iY2;
      if((ycon1) && (ycon2)){
        currentY+= nextCY;
      }else{
        if(!ycon2) {
          // currentX -= (currentX + nextCX - (iX1 + lines.strokeWidth + 1)) - nextCX;
          // print("HWO");
        }
      }
    }
    isYMoveAble = true;
    isXMoveAble = true;
    if((cY<-2 || cY>2) || (cX<-2 || cX>2)){
      frameNum = (frameNum == 3) ? 0 :frameNum+1;
    }else{
      frameNum = 0;
    }

  }

  List<ui.Image> imagus;
  Game(this.accuX, this.accuY, this.imagus);
  static int  score = 0, death = 0;
  int lastTUpdate = 0;
  var goal = Paint()
    ..color = Colors.black
    ..strokeWidth = 10.0;
  var  ball = Paint()
    ..color = Colors.black
    ..strokeWidth = 8.0;
  var  lines = Paint()
    ..color = Colors.black
    ..strokeWidth = 5.0;

  static bool isNewOpen = true;
  double linesAX, linesAY, linesBX, linesBY;
  static String statusQ = "";
  @override
  void paint(Canvas canvas, Size size) {
    bigMe = size;
    linesOffset.add(new NewOffset(new Offset(size.width * 0.2, size.height * 0.2), new Offset(size.width * 0.2, size.height * 0.8)));
    linesOffset.add(new NewOffset(new Offset(size.width * 0.5, size.height * 0.2), new Offset(size.width * 0.5, size.height * 0.8)));
    linesOffset.add(new NewOffset(new Offset(size.width * 0.2, size.height * 0.2), new Offset(size.width * 0.5, size.height * 0.2)));
    linesOffset.add(new NewOffset(new Offset(size.width * 0.5, size.height * 0.8), new Offset(size.width * 1, size.height * 0.8)));
    Offset tmpPoints = Offset(0, 0);
    if(death != 3){
      moveBall(size, accuX,accuY);
      isYMoveAble = true;
      isXMoveAble = true;
      tmpPoints = Offset(currentX, currentY);
    }



    List<NewOffset> waters = new List<NewOffset>();
    waters.add(NewOffset(Offset(size.width * 0.8, size.height * 0.2), Offset(size.width * 1, size.height * 0.4)));
    waters.add(NewOffset(Offset(size.width * 0.5, size.height * 0.2), Offset(size.width * 0.7, size.height * 0.4)));
    // for(int i=0; i<waters.length; i++){
    //   canvas.drawRect( Rect.fromPoints(waters.elementAt(i).a1, waters.elementAt(i).a2), lines);
    // }
    ///
    /// HIT POINT
    ///
    if (tmpPoints.dx < size.width * 0.6 + 10 && tmpPoints.dx>size.width*0.6-10
        && tmpPoints.dy < size.height / 2 + 10 && tmpPoints.dy>size.height / 2-10) {
      // print("HIT");
      statusQ = "YOU WIN!";
      currentX = bigMe.width/2.5;
      currentY = bigMe.height/2.5;
      score++;
    }
    for(int i=0; i<waters.length; i++){
      if(tmpPoints.dx>waters.elementAt(i).a1.dx && tmpPoints.dx<waters.elementAt(i).a2.dx
       && tmpPoints.dy>waters.elementAt(i).a1.dy && tmpPoints.dy<waters.elementAt(i).a2.dy){
        statusQ = "YOU DIED!";
        currentX = bigMe.width/2.5;
        currentY = bigMe.height/2.5;
        death++;
      }
    }


    //MAKE SCENE

    Rect bggrass = const Offset(0.0, 0.0) & const Size(50.0, 33.0);
    for(int i=0; i<(size.height/33).round()+1; i++){
      for(int ii=0; ii<(size.width/50).round()+1; ii++){
        canvas.drawImageRect(imagus.elementAt(1), bggrass, bggrass.shift(Offset((49*ii).toDouble(), (32*i).toDouble())), Paint());
      }
    }

    Rect wotah =  Offset(0, 0) & const Size(21.0, 16.0);
    for(int i=0; i<((size.height * 0.2)/15)-1; i++){
      for(int ii=0; ii<(size.width*0.2)/20;ii++){
        canvas.drawImageRect(imagus.elementAt(4), wotah, wotah.shift(Offset(size.width * 0.8+ (ii.toDouble()*20).toDouble(), (size.height * 0.2)+(15*i).toDouble())), Paint());
      }
      for(int ii=0; ii<(size.width*0.15)/20;ii++) {
        canvas.drawImageRect(imagus.elementAt(4), wotah,wotah.shift(Offset((size.width * 0.5) + (ii.toDouble() * 20), (size.height * 0.2) + (15 * i).toDouble())), Paint());
      }
    }

    //draw character
    int chwhere = 0;
    double chOX = tmpPoints.dx-14;
    double chOY = tmpPoints.dy-13;
    if(accuX.toInt().abs()>accuY.toInt().abs()){
      //either left or right
      if(accuX>0){
        //left
        chwhere = 2;
        chOY = chOY - 100;
      }else{
        //right
        chwhere = 3;
        chOY = chOY - 150;
      }
    }else{
      //either go up or down
      if(accuY>0){
        chwhere = 0;
        //down
      }else{
        chwhere = 1;
        chOY = chOY - 50;
        //up
      }
    }
    List<Rect> ch= new List<Rect>();
    ch.add(Offset(0.0, (chwhere.toDouble() * 50)) & const Size(50.0, 50.0));
    ch.add(Offset(31.0, (chwhere.toDouble() * 50)) & const Size(50.0, 50.0));
    ch.add(Offset(101.0, (chwhere.toDouble() * 50)) & const Size(50.0, 50.0));
    ch.add(Offset(130.0, (chwhere.toDouble() * 50)) & const Size(50.0, 50.0));
    List<Offset> cho = new List<Offset>();
    cho.add(Offset(-40+chOX,-10+chOY));
    cho.add(Offset(-58+chOX,-10+chOY));
    cho.add(Offset(-36+chOX,-10+chOY));
    cho.add(Offset(-54+chOX,-10+chOY));
    canvas.drawImageRect(imagus.elementAt(3), ch.elementAt(frameNum), ch.elementAt(1).shift(cho.elementAt(frameNum)), Paint());
    // canvas.drawCircle(tmpPoints, 5, ball); //use for debugging

    //fences
    Rect ls = const Offset(1.0, 0.0) & const Size(10.0, 46.0);
    Rect pt = const Offset(15.0, 45.0) & const Size(64.0, 64.0);
    for(int i=0; i<linesOffset.length; i++){
      //check if potrait or lanscape
      double AX = linesOffset.elementAt(i).a1.dx;
      double AY = linesOffset.elementAt(i).a1.dy;
      double BX = linesOffset.elementAt(i).a2.dx;
      double BY = linesOffset.elementAt(i).a2.dy;
      if(AY == BY){
        //if potrait
        for(int ii=0; ii<(BX-AX)/45; ii++){
          canvas.drawImageRect(imagus.elementAt(0),pt,pt.shift(Offset(AX+(45*(ii.toDouble())-22),AY-60)), Paint());
        }
      }else{
        //if landscape
        for(int ii=0; ii<(BY-AY)/46; ii++) {
          canvas.drawImageRect(imagus.elementAt(0), ls, ls.shift(Offset(AX - 7, AY+(45*ii))), Paint());
          // canvas.drawImageRect(imagus, ls, ls.shift(Offset(AX - 7, AY)), Paint());
        }
      }
      // canvas.drawLine(linesOffset.elementAt(i).a1,linesOffset.elementAt(i).a2, lines); // for debugging
    }

    //goal /chest
    Rect chest = const Offset(0.0, 10.0) & const Size(50.0, 25.0);
    canvas.drawImageRect(imagus.elementAt(2),chest,chest.shift(Offset((size.width * 0.6)-25, (size.height / 2)-25)), Paint());
    // canvas.drawCircle(new Offset(size.width * 0.6, size.height / 2), 10, goal); //only for debugging

    //score
    ui.ParagraphBuilder btmText = new ui.ParagraphBuilder(ui.ParagraphStyle(fontSize: 16.8, fontWeight: FontWeight.bold))
      ..pushStyle(new ui.TextStyle(color: Colors.black))
      ..addText('Score: $score\nDeath: $death');
      // ..addText('\nX: ${accuX.toInt()}, Y: ${accuY.toInt()}');

    canvas.drawParagraph(btmText.build()
      ..layout(new ui.ParagraphConstraints(width: size.width / 2)), new Offset(20, 50));

    if(isNewOpen) {
      isNewOpen = false;
      statusQ = "WELCOME!";
    }

    if(death == 3){
      statusQ = "YOU LOSE";
    }
    ui.ParagraphBuilder cText = new ui.ParagraphBuilder(ui.ParagraphStyle(fontSize: 50, fontWeight: FontWeight.bold))
      ..pushStyle(new ui.TextStyle(color: Colors.black))
      ..addText(statusQ);
    if(statusQ.isNotEmpty){
      frameText = frameText+1;
      if(frameText > 20) {
        statusQ = "";
        frameText = 0;
      }
      canvas.drawParagraph(cText.build()
        ..layout(new ui.ParagraphConstraints(width: size.width)), new Offset(size.width / 2 - 100, size.height / 2.5));
    }
    // canvas.save();
    // canvas.restore();
  }
  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
  void greatReset(){
    currentX = bigMe.width/2.5;
    currentY = bigMe.height/2.5;
    score = 0;
    death = 0;
  }
}