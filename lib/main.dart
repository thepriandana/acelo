import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:grd_accelerometer_games/game.dart';
import 'package:sensors/sensors.dart';
import 'dart:ui' as ui;


void main(){
  runApp(new MaterialApp(
    routes: <String, WidgetBuilder>{
      '/': (BuildContext context) => MyApp(),
    },
  ));
}

class _State extends State<MyApp>{
  var _accuX = 0.0, _accuY = 0.0;
  int lastUpdate = 0;
  List<ui.Image> _image;
  List<String> listAsset = new List<String>();

  _loadImage() async {
    for(int i=0; i<listAsset.length; i++){
      ByteData bd = await rootBundle.load(listAsset.elementAt(i));
      final Uint8List bytes = Uint8List.view(bd.buffer);
      final ui.Codec codec = await ui.instantiateImageCodec(bytes);
      final ui.Image image = (await codec.getNextFrame()).image;
      setState(() => _image.add(image));
    }
  }
  @override
  Widget build(BuildContext context) {
    Game game = Game(_accuX, _accuY, _image);
    return new Scaffold(
        appBar: new AppBar(
          title: new Text('ACELO'),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        floatingActionButton:
        new FloatingActionButton.extended(
          label: new Text("Reset"),
          onPressed: (){game.greatReset();} ,
        ),
        body: new CustomPaint(
          size: Size.infinite,
          painter: game,
        ),
    );
  }

  @override
  void initState() {
    super.initState();
    _image = new List<ui.Image>();
    listAsset.add("Assets/Tilesets/Fences.png");
    listAsset.add("Assets/Tilesets/Grass.png");
    listAsset.add("Assets/Objects/Chest.png");
    listAsset.add("Assets/Characters/BasicCharakterSpritesheet.png");
    listAsset.add("Assets/Tilesets/Water.png");
    _loadImage();

    accelerometerEvents.listen((AccelerometerEvent event) {
      if (this.mounted) {
        setState(() {
          int curTime = DateTime
              .now()
              .microsecondsSinceEpoch;
          if ((curTime - lastUpdate) > 100) {
            lastUpdate = curTime;
            _accuY = event.y;
            _accuX = event.x;
          }
        });
      }
    });
  }

}
class MyApp extends StatefulWidget{
  @override
  _State createState() => new _State();
}