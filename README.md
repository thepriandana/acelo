# Acelo
A simple game using an accelerometer sensor.

##Screenshot
![Aclio demo on mobile phone](Aclio.png)

## Description
This program use Accelerometer sensor on mobile devices.
The main logic is to make an awarness of the character with their surronding by make an active area and restrain it to other side of a area. This method can be simply implemented by creating a square like area which have 4 side (Upper, Right, Bottom, Left) as the maximum reached area. This logic may be implemented to make an awarness of multiple drone to restrain from crashing with each other.

## Installation
This game can be install by running the apk installation in output folder (For android devices, for iOS please download android studio, then build & run the project).

## Roadmap
This game may be updated in the future.

## Authors and acknowledgment
Special thanks to Cup Nooble for the asset.

## License
    Acelo - A simple game using an accelerometer sensor.
    Copyright (C) 2022  ThePriandana